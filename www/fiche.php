<?php
  include 'functions/functions.php';
  $fontName = $_GET['font'];
  $fontDir = '../output/'.$fontName;
?>

<script src="js/opentype.js"></script>

  <div class="box-fiche" data-fontdir="<?= $fontName; ?>" >
  <textarea class="testing hd" style="font-family: 'Hershey-Noailles-<?= $fontName ?>'">Hershey</textarea>

  <?php 
    include('inc/glyph-inspector.php');
  ?>
    <div class="text-speci" style="font-family: 'Hershey-Noailles-<?= $fontName ?>'" >

        <p class="sizelabel">60px</p>
        <p class="textline" contenteditable="true" style="font-size: 60px; line-height: normal">Arrowroot Barley Chervil Dumpling Endive Flaxseed Garbanzo Hijiki Ishtu Jicama Kale Lychee Marjoram Nectarine Oxtail Pizza Quinoa Roquefort Squash Tofu Uppuma Vanilla Wheat Xergis Yogurt Zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>

        <p class="sizelabel">48px</p>
        <p class="textline" contenteditable="true" style="font-size: 48px; line-height: normal">Arrowroot Barley Chervil Dumpling Endive Flaxseed Garbanzo Hijiki Ishtu Jicama Kale Lychee Marjoram Nectarine Oxtail Pizza Quinoa Roquefort Squash Tofu Uppuma Vanilla Wheat Xergis Yogurt Zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>
        
        <p class="sizelabel">36px</p>
        <p class="textline" contenteditable="true" style="font-size: 36px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>
        
        <p class="sizelabel">30px</p>
        <p class="textline" contenteditable="true" style="font-size: 30px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>
        <p class="sizelabel">30px</p>
        <p class="textline" contenteditable="true" style="font-size: 30px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>

        <p class="sizelabel">24px</p>
        <p class="textline" contenteditable="true" style="font-size: 24px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>

        <p class="sizelabel">20px</p>
        <p class="textline" contenteditable="true" style="font-size: 20px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>
        <p class="sizelabel">18px</p>
        <p class="textline" contenteditable="true" style="font-size: 16px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>
        <p class="sizelabel">14px</p>
        <p class="textline" contenteditable="true" style="font-size: 14px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>
        <p class="sizelabel">12px</p>
        <p class="textline" contenteditable="true" style="font-size: 12px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>
        
        <p class="sizelabel">6px</p>
        <p class="textline" contenteditable="true" style="font-size: 6px; line-height: normal">arrowroot barley chervil dumpling endive flaxseed garbanzo hijiki ishtu jicama kale lychee marjoram nectarine oxtail pizza quinoa roquefort squash tofu uppuma vanilla wheat xergis yogurt zweiback 0 1 2 3 4 5 6 7 8 9 ! ?</p>
    </div>


</div>
  <nav>
      <h1><?= $fontName; ?></h1>
    <div class="download">
      <h2>Download Files</h2>
      <?php
        ScanDirectoryDl($fontDir); 
      ?>
      <hr>
      <h2>Links</h2>
        <pre>
          <code><?php MakeWebFont($fontDir); ?></code>
      </pre>
    </div>
  </nav>  
