#!/usr/bin/env bash

font=$1
echo $font
mkdir output-svg/$font/over

for gly in output-svg/$font/*.svg;
do
  cp $gly output-svg/$font/over/
  file=`basename $gly .svg`

  inkscape --verb EditSelectAllInAllLayers \
          --verb SelectionUnGroup \
          --verb ObjectToPath \
          --verb StrokeToPath \
          --verb SelectionUnion \
          --verb SelectionReverse \
          --verb FileSave \
          --verb FileClose \
          --verb FileQuit \
    output-svg/$font/over/$file.svg \

  # echo $file

  # inkscape $gly --export-ps="simple/$file.ps"
  # # inkscape "simple/$file.eps" --export-plain-svg="simple/$file.svg"
  # # rm simple/$file.esp
done


