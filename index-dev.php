<!DOCTYPE html>
<html>
  <head>
    <title>Hershey-Noailles</title>
    <meta charset="UTF-8">
    <link href="style/main-dev.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-3.2.1.min.js"></script>
  </head>
  <body>

    <div id="cont_testing">
      <div id="viewer">

      </div>
    </div>
    <nav>
      <table>
        <tr>
          <caption>
                |text <input id="font-text" value="_all_" type="text">
                |zoom <input data-attri="css" data-propri="height" min="0" max="1200" value="600" name="discount_credits" class="discount_credits" id="cadra" type="range">
                <span id="btnMenu">+</span>
          </caption>
          <td>size</td>
          <td>width</td>

        </tr>
        <tr>
          <th>Json</th>
          <th>info</th>
          <th>table</th>
          <th>style input</th>
        </tr>
         <tr>
          <td>
            <?php
            $dossier = 'JSON/';
            $iterator = new DirectoryIterator($dossier);
            foreach($iterator as $fichier){
              if(!$fichier->isDot()){
            ?>
              <a class="jsonfile" href="#"><?php echo $fichier->getFilename(); ?></a><br>
            <?php
              }
            }
            ?>
          </td>
          <td id="tableInfo">
            name: <span id="name"></span><br>
            date: <span id="date"></span><br>

          </td>
          <td id="tableChar">
            <form action="write.php" method="post">
              <input id="fontName" name="fontName" value="fontname" type="text">
              <input value="save" type="submit">

            </form>
          </td>
          <td>
            <textarea id="editor" cols="30" rows="10"></textarea>
          </td>

        </tr>

      </table>
    </nav>

  <script type="text/javascript">
        var file = location.hash.substring(1);
        var chasse = 1;
				var strokeWidth = 2;
				var monoChasse = false;
				var style = "fill: none; stroke: #000000; stroke-width: "+strokeWidth+"; stroke-linecap: round; stroke-linejoin: round"
        $('#editor').html(style);

        function viewer(data, output, text, cadra){

					style = $('#editor').html()

          if (output == '#viewer'){
            $(output).html('');
          };

            $.getJSON('JSON/'+data, function(fonts){

              var fontName = fonts.fontname;
              var fontDate= fonts.data-date;
              $('nav #tableInfo #name').html(fontName);
              $('nav #tableInfo #date').html(fontDate);
              window.location.hash = data;

              if(text == '_all_'){
                  var textTable = [];
                  for (var i = 0; i < fonts.chars.length; i++) {
                    textTable[i] = String.fromCharCode(fonts.chars[i].dec);
                  }
                textTable.sort();
              }else{
                var textTable = text.split('');
              }

              for (var i = 0; i < textTable.length; i++) {
                var chars = textTable[i];
                var dec = chars.charCodeAt();
                var pt = [];

                var path = function(option){
                  for (var u = 0; u < fonts.chars.length; u++){
                      if (dec == fonts.chars[u].dec){
                        if (monoChasse != false){
                          var fontWidth = monoChasse;
                          var scalePropo = monoChasse / fonts.chars[u].width;
                        }else {
                          var fontWidth = fonts.chars[u].width;
                          var scalePropo = chasse;
                        }
                        pt['height'] = fonts.height;
                        pt['width'] = fontWidth;
                        pt['d'] = fonts.chars[u].d;
                        pt['scale'] = scalePropo;
// -------------------------------------------------------------
                        // pt['style'] = "fill: none; stroke: #000000; stroke-width: "+strokeWidth+"; stroke-linecap: round; stroke-linejoin: round";
                        pt['style'] = style;
                        // -------------------------------------------------------------
                        // $('#editor').html(pt['style']);
                        return pt[option];
                      }
                    }
                  };
                var composeSvg = function(){
                  pathBuild = $('<path/>', {
                    d: path('d'),
                    style: path('style'),
                    transform: 'scale('+path('scale')+',1)',
                });
                  svgBuild = $('<svg/>',{
                        'data-dec': dec,
                        html: pathBuild[0].outerHTML
                      }).attr({
                        width: parseInt(path('width') * chasse),
                        height: path('height')
                      });
                  return svgBuild[0].outerHTML;
                };

                $('<'+cadra+'/>',{
                  name: dec,
                  id: 'cadratin_'+i,
                  class: 'cadratin',
                  html: composeSvg
                }).appendTo(output);

                $('<div/>',{
                  class: 'info',
                }).appendTo(output+' #cadratin_'+i);
              }
            })
        };

        $('#font-text').keyup(function(){
          var textInput = $(this).val();
          var file = location.hash.substring(1);
          viewer(file, '#viewer', textInput, 'div');
        })

        $('#editor').keyup(function(){

          var stylePt = $(this).val();
          var paths = $('path');
          paths.each(function(){
            $(this).attr('style', stylePt);
						console.log($(this))
          })
          viewer(file, '#tableChar form', '_all_', 'textarea');
        })

        function loadJSON(){
        $('a.jsonfile').click(function(){

          var file = $(this).html();
          var textInput = $('#font-text').val();
          viewer(file, '#viewer', textInput, 'div');
          viewer(file, '#tableChar form', '_all_', 'textarea');
          return file;
        })
      }

     $('.discount_credits').on('change mousemove', function() {
        var valScale = $(this).val()/100;

       console.log(valScale);
       $('#viewer').css({
        transform: 'scale('+valScale+')',
        width: 100 / valScale +'%',

       });

    });

      loadJSON();
      viewer(file, '#viewer', '_all_', 'div');
      viewer(file, '#tableChar form', '_all_', 'textarea');

      // GUI

      $('#btnMenu').click(function(){
        $('tbody').toggle()
      })


  </script>

  </body>
</html>
