# /usr/bin/env python2

import glob
import sys
import lxml.etree as ElementTree
import json
from datetime import date

today = date.today()
folder = sys.argv[1]
dataLetters = []

dirFiles = 'output-svg/'+folder+"/*.svg"
for files in glob.glob(dirFiles):
    print(files)
    with open(files, 'rt') as f:
        tree = ElementTree.parse(f)

    root = tree.getroot()

    letterDec = root.attrib['data-dec']
    letterWidth = root.attrib['width']

    for path in root.iter():
        path_d = path.attrib.get('d')
        if path_d:
            letterD = path_d
            print(letterD)

    dataLetters += [{'dec': letterDec, 'width': letterWidth, 'd': letterD}]


data = {
   'fontname': folder,
   'height': 40,
   'data-date': 'la date',
   'chars': dataLetters
}


with open('JSON/'+folder+'.json', 'w') as f:
    json.dump(data, f, indent=2)

print(data)
