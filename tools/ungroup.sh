#!/usr/bin/env bash


font=$1
echo $font
rm -f tmp/path/*.svg

for gly in output-svg/$font/*.svg;
do
  file=`basename $gly .svg`

  inkscape --verb EditSelectAllInAllLayers \
          --verb ObjectToPath \
					--verb SelectionGroup \
					--verb SelectionUnGroup \
          --verb SelectionCombine \
          --verb FileSave \
          --verb FileClose \
          --verb FileQuit \
  $gly \

done



