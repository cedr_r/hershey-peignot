#/usr/bin/env python2
import glob
import sys
import math
import lxml.etree as ET


folderName = sys.argv[1]
print(folderName)
SVG_DIR = glob.glob('output-svg/' + folderName + '/*.svg')

for glyph in SVG_DIR:
    print(glyph)
    with open(glyph, 'rt') as f:
        tree = ET.parse(f)
    root = tree.getroot()
    svgDec = root.attrib.get('data-dec')
    newPath = []
    for path in root.iter():
        path_d = path.attrib.get('d')

        if path_d:
            path_s = path
            print(path_d)
            print('---------------')
            p = path_d.split(' ')
            for elemp in p:

                if elemp.find('.') != -1 and elemp.find(',') != -1:
                    print(elemp + '--> Dirty (. and ,)')
                    v = elemp.split(',')
                    newPath += str(round(float(v[0])))
                    newPath += ','
                    newPath += str(round(float(v[1])))

                elif elemp.find('.') != -1:
                    newPath += str(round(float(elemp)))

                else:
                    newPath += str(elemp)
                    print(elemp + '--> Clean')

                newPath += ' '
            path.set('d', ''.join(newPath))
            path.attrib.pop('{http://www.inkscape.org/namespaces/inkscape}original-d', None)


    tree.write('output-svg/' + folderName + '/' + svgDec +'.svg')
    print(''.join(newPath))
    print('!!!!!!!!!!!!!!END \n \n-------------')
